import style from './Navbar.module.css'
import React from "react";
import {NavLink} from "react-router-dom";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from '@material-ui/core/List';
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {Dashboard} from "@material-ui/icons";

const Navbar = () => {
    return (
        <List component="nav" className={style.nav} aria-label="mailbox folders">
            <ListItem button divider component={NavLink} to="/tasks">
                <ListItemIcon>
                    <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Задачи"/>
            </ListItem>
            <ListItem button divider component={NavLink} to="/projects">
                <ListItemIcon>
                    <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Проекты"/>
            </ListItem>
            <ListItem button divider component={NavLink} to="/graphics">
                <ListItemIcon>
                    <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Графики"/>
            </ListItem>
            <ListItem button divider component={NavLink} to="/settings">
                <ListItemIcon>
                    <Dashboard/>
                </ListItemIcon>
                <ListItemText primary="Настройка"/>
            </ListItem>
        </List>
    );
}
export default Navbar;
