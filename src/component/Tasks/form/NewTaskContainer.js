import {connect} from "react-redux";
import {addTaskAC} from "../../../redux/reducer/TaskReducer";
import NewTask from "./NewTask";


const mapStateToProps = (state) => {
    return {
     taskForm: state.form.taskForm
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addTask: (task) => {
            dispatch(addTaskAC(task))
        }
    }
}
const NewTaskContainer = connect(mapStateToProps,mapDispatchToProps)(NewTask);
export default NewTaskContainer;