import React from "react";
import {Field, reduxForm} from 'redux-form'

const TaskForm = (props) => {
    const {handleSubmit} = props
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label>Название</label>
                <Field component="input" name='title' type="text"/>
            </div>
            <div>
                <label>Описание</label>
                <Field component="input" name='description' type="text"/>
            </div>
            <div>
                <label>Дата начала</label>
                <Field component="input" name='dateBegin' type="date"/>
            </div>
            <div>
                <label>Дата окончания</label>
                <Field component="input" name='dateEnd' type="date"/>
            </div>
            <button>Add task</button>
        </form>
    )
}

const TaskReduxForm = reduxForm({form: 'taskForm'})(TaskForm)

const NewTask = (props) => {
    const onSubmit = () => {
        props.addTask(props.taskForm.values);
    }
    return (
        <div>
            <TaskReduxForm onSubmit={onSubmit}/>
        </div>
    )
}

export default NewTask;