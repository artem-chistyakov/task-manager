import React from "react";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {NavLink} from "react-router-dom";
import Button from "@material-ui/core/Button";


class Tasks extends React.Component {

    componentDidMount() {
        this.props.setTasks();
    }

    render() {
        return (
            <div>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Название задачи</TableCell>
                                <TableCell align="center">Описание</TableCell>
                                <TableCell align="center">Время начала</TableCell>
                                <TableCell align="center">Время конца</TableCell>
                                <TableCell align="center">Тип</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.tasks.map((task) => (
                                <TableRow key={task.id}>
                                    <TableCell component="th" scope="row">{task.title}</TableCell>
                                    <TableCell align="center">{task.description}</TableCell>
                                    <TableCell align="center">{task.dateBegin}</TableCell>
                                    <TableCell align="center">{task.dateEnd}</TableCell>
                                    <TableCell align="center">{task.type}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <Button component={NavLink} to='/newTask'>Add task</Button>
            </div>
        );
    }
}

export default Tasks;