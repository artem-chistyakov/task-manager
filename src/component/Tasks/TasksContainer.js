import {connect} from "react-redux";
import Tasks from "./Tasks";
import {addTaskAC, setTasksAC} from "../../redux/reducer/TaskReducer";

let mapStateToProps = (state) => {
    return {
        tasks: state.tasksPage.tasks
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setTasks: () => {
            dispatch(setTasksAC())
        },
        addTask: (task) => {
            dispatch(addTaskAC(task))
        }
    }
}
const TasksContainer = connect(mapStateToProps, mapDispatchToProps)(Tasks);
export default TasksContainer;