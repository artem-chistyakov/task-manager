import React from "react";
import Button from "@material-ui/core/Button";
import {keycloak} from './../../index';
import styles from './Header.module.css'
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}));

const Header = () => {

    const classes = useStyles();

    return (
        <div className={styles.header}>
            <div className={styles.logo}>
                Task Manager
            </div>
            <div>
                <header/>
            </div>
            <div>
                <Button
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    startIcon={<ExitToAppIcon/>}
                    onClick={keycloak.logout}
                >
                    LOGOUT
                </Button>
            </div>
        </div>
    )
}
export default Header;