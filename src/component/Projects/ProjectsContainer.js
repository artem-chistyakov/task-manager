import {connect} from "react-redux";
import Projects from "./Projects";
import {setProjectsAC} from "../../redux/reducer/ProjectReducer";

const mapStateToProps = (state) => {
    return {
        projects: state.projectsPage.projects
    }
};
const mapDispatchToProps = (dispatch) => {
 return {
     setProjects: () => {
         dispatch(setProjectsAC())
     }
 }
};
const ProjectsContainer = connect(mapStateToProps,mapDispatchToProps)(Projects);
export default ProjectsContainer;