import React from "react";
import Project from "./Project/Project";
import styles from './Projects.module.css'
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";

class Projects extends React.Component {

    componentDidMount(): void {
        debugger;
        this.props.setProjects();
    }

    render() {
        return (
            <div>
                <div className={styles.Projects}>
                    {this.props.projects.map(
                        p => {
                            return (
                                <div className={styles.Project}>
                                    <Project/>
                                </div>
                            )
                        })}
                </div>
                <div>
                    <Button component={NavLink} to='/NewProject'>Add New Project</Button>
                </div>
            </div>

        )
    }

}

export default Projects;