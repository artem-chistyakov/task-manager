import React from "react";
import {Field, reduxForm} from "redux-form";

const ProjectForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <label>Название</label>
                <Field component='input' type='text' name='title'/>
            </div>
            <div>
                <label>Описание</label>
                <Field component='input' type='text' name='description'/>
            </div>
            <div>
                <label>Дата начала</label>
                <Field component='input' type='date' name='dateBegin'/>
            </div>
            <div>
                <label>Дата окончания</label>
                <Field component='input' type='date' name='dateEnd'/>
            </div>
            <button>Создать</button>
        </form>
    )
}
const ProjectReduxForm = reduxForm({form: 'projectForm'})(ProjectForm)

const NewProject = (props) => {
    const onSubmit = () => {
        props.addProject(props.projectForm.values)
    }
    return (
        <div>
            <ProjectReduxForm onSubmit={onSubmit}/>
        </div>
    )
}


export default NewProject;