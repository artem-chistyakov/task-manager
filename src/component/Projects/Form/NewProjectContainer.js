import {addProjectsAC} from "../../../redux/reducer/ProjectReducer";
import NewProject from "./NewProject";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
    return {
        projectForm: state.form.projectForm
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addProject: (project) => {
            dispatch(addProjectsAC(project))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProject);