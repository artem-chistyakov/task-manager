import ProjectReducer from "./reducer/ProjectReducer";
import TaskReducer from "./reducer/TaskReducer";
import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import { reducer as formReducer } from 'redux-form'

let reducers = combineReducers({
    tasksPage: TaskReducer,
    projectsPage: ProjectReducer,
    form: formReducer
})
let store = createStore(reducers, applyMiddleware(thunk));

export default store;