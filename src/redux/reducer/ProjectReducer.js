import Axios from "../../util/Axios";

const setProjects = 'SET_PROJECTS';
const addProject = 'ADD_PROJECT';

let projectsPage = {
    projects: []
}

const ProjectReducer = (state = projectsPage, action) => {
    switch (action.type) {
        case setProjects:
            if(action.projects != null)  return {
                ...state,
                projects: action.projects
            }
            else return {
                ...state
            }
        case addProject:
            return {
                ...state,
                projects: [...state.projects, action.project]
            }
        default: {
            return state;
        }
    }
}

export const setProjectsAC = () => {
    return (dispatch) => {
        Axios.get("/projects",{
            headers: {'Authorization': `Bearer ${localStorage.getItem('keycloak-token')}`}
        })
            .then(response => {
                dispatch(setProjectsSuccessAC(response.data))
            }).catch(error => {
            throw (error)
        });
    }
}
export const setProjectsSuccessAC = (projects) => {
    return {
        type: setProjects,
        projects
    }
}
export const addProjectsAC = (project) => {
    return (dispatch) => {
        Axios.post('/projects', project,{
            headers: {'Authorization': `Bearer ${localStorage.getItem('keycloak-token')}`}
        })
            .then(response => dispatch(addProjectsSuccessAC(project)))
            .catch(error => {
            throw (error)
        });
    }

}
export const addProjectsSuccessAC = (project) => {
    return {
        type: addProject,
        project
    }

}


export default ProjectReducer;