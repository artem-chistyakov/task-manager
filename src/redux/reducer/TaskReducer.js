import Axios from "../../util/Axios";

const addTask = 'ADD_TASK';
const setTask = 'SET_TASKS';

let taskPage = {
    tasks: [],
}
const TaskReducer = (state = taskPage, action) => {
    switch (action.type) {
        case addTask: {
            return {
                ...state,
                tasks: [...state.tasks,action.task]
            }
        }
        case setTask: {
            if(action.tasks != null) return {
                ...state,
                 tasks: [...action.tasks]
            }
            else  return {
                ...state
            }
        }
        default:
            return state;
    }
}
export default TaskReducer;


export const addTaskAC = (task) => {
    return (dispatch) => {
        return Axios.post('/tasks', task,{
            headers: {'Authorization': `Bearer ${localStorage.getItem('keycloak-token')}`}
        })
            .then(response => {
                dispatch(addTaskSuccessAC(task))
            })
            .catch(error => {
                throw (error)
            });
    };
};
export const setTasksAC = ()=>{
    return dispatch => {
        return Axios.get('/tasks',{
            headers: {'Authorization': `Bearer ${localStorage.getItem('keycloak-token')}`}
        })
            .then(response => {
                dispatch(setTasksSuccessAC(response.data))
            }).catch(error => {
                throw (error)
            });
    }
}
export const setTasksSuccessAC = (tasks) => ({type: setTask, tasks});
export const addTaskSuccessAC = (task) => ({type: addTask,task});
