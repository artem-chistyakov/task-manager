import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import Keycloak from "keycloak-js";
import { createBrowserHistory } from "history";

const customHistory = createBrowserHistory();

export const keycloak = Keycloak('/keycloak.json');

keycloak.init({onLoad: 'login-required'}).then((auth) => {

    if (!auth) {
        window.location.reload();
    } else {
        console.info("Authenticated");
    }

ReactDOM.render(
    <BrowserRouter history={customHistory}>
        <Provider store={store}>
            <React.StrictMode>
                <App/>
            </React.StrictMode>
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);
    localStorage.setItem("keycloak-token", keycloak.token);
}).catch(() => {
    console.error("Authenticated Failed");
});

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
