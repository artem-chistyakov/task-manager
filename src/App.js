import React from 'react';
import './App.css';
import Header from "./component/Header/Header";
import Navbar from "./component/Navbar/Navbar";
import {Route} from "react-router";
import TasksContainer from "./component/Tasks/TasksContainer";
import MyTable from "./component/Table/MyTable";
import NewTaskContainer from "./component/Tasks/form/NewTaskContainer";
import ProjectsContainer from "./component/Projects/ProjectsContainer";
import NewProjectContainer from "./component/Projects/Form/NewProjectContainer";


function App() {
    return (
        <div className="App">
            <Header/>
            <Navbar/>
            <div className='app-wrapper-content'>
                <Route path="/tasks" render={() => <TasksContainer/>}/>
                <Route path="/graphics" render={() => <MyTable/>}/>
                <Route path="/settings" render={() => <TasksContainer/>}/>
                <Route path="/newTask" render={() => <NewTaskContainer/>}/>
                <Route path="/projects" render={() => <ProjectsContainer/>}/>
                <Route path="/newProject" render={() => <NewProjectContainer/>}/>
            </div>
        </div>
    );
}

export default App;
