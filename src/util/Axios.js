import Axios from 'axios';

export default Axios.create({
// baseURL: 'http://0.0.0.0:8080/api',  //for develop
    baseURL: 'https://taskmanager.zoach.ru/api',     //for production
    responseType: 'json'
})
